if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
  require("dotenv").config();
}

const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");
const cors = require("cors");
const logger = require("morgan");
const path = require("path");

const authRouter = require("./routers/authRouter");
const userRouter = require("./routers/userRouter");
const boardRouter = require("./routers/boardRouter");
const columnRouter = require("./routers/columnRouter");
const cardRouter = require("./routers/cardRouter");
const commentRouter = require("./routers/commentRouter");

const PORT = process.env.PORT || 8080;

const app = express();

app.use(passport.initialize());
require("./middleware/passport")(passport);

app.use(express.json());
app.use(cors());
app.use(logger("combined"));

app.use("/api/auth", authRouter);
app.use(
  "/api/users",
  passport.authenticate("jwt", { session: false }),
  userRouter
);
app.use(
  "/api/boards",
  passport.authenticate("jwt", { session: false }),
  boardRouter
);
app.use(
  "/api/columns",
  passport.authenticate("jwt", { session: false }),
  columnRouter
);
app.use(
  "/api/cards",
  passport.authenticate("jwt", { session: false }),
  cardRouter
);
app.use(
  "/api/comments",
  passport.authenticate("jwt", { session: false }),
  commentRouter
);

app.use(express.static(path.resolve(__dirname, `dist/client`)));

app.get("/*", function (req, res) {
  res.sendFile(path.resolve(__dirname, `dist/client/index.html`));
});

app.use((req, res) => {
  res.status(404).send({ message: "Page not found" });
});

app.use((req, res) => {
  res.status(500).send({ message: "Server error" });
});

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URL);
    app.listen(PORT, () => console.log(`Server running on ${PORT} port...`));
  } catch (err) {
    console.error(err.message);
  }
};

start().then(() => console.log("System started"));
