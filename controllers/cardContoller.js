const { User } = require("../models/userModel");
const { Board } = require("../models/boardModel");
const { Column } = require("../models/columnModel");
const { Card } = require("../models/cardModel");

class CardController {
  async createCard(req, res) {
    try {
      const cardText = req.body.cardText;
      const board = req.body.board;
      const userId = req.user._id;
      const columnId = req.params.id;
      const email = req.user.email;

      const newCard = new Card({
        cardText,
        column: columnId,
        board,
        email,
        createdBy: userId,
        createdDate: Date.now(),
      });

      await newCard.save();

      const column = await Column.findOne({ _id: columnId });

      column.cards.push(newCard);

      await column.save();

      return res.status(200).send(newCard);
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getCards(req, res) {
    const boardId = req.params.id;

    const cards = await Card.find({ search: true });

    if (!cards) {
      return res.status(409).send({ message: `cards not found` });
    }

    return res.status(200).send(cards);
  }
  catch(err) {
    res.status(400).send({ message: err.message });
  }

  async deleteCard(req, res) {
    try {
      const userId = req.user._id;
      const cardId = req.params.id;

      const checkCard = await Card.findOneAndRemove({
        _id: cardId,
        createdBy: userId,
      });

      if (!checkCard) {
        return res.status(409).send({ message: `This board not found` });
      }

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async updateCard(req, res) {
    try {
      const cardId = req.params.idCard;
      const newCardColumn = req.params.idColumn;

      await Card.findOneAndUpdate({ _id: cardId }, { column: newCardColumn });

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }
}

module.exports = new CardController();
