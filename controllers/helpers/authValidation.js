const Joi = require('joi');

const authValidator = Joi.object({
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string()
    .min(4)
    .max(16)
    .required(),
  username: Joi.string()
    .required(),
});

module.exports = {
  authValidator,

};
