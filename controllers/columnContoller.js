const { Board } = require("../models/boardModel");
const { User } = require("../models/userModel");
const { Column } = require("../models/columnModel");
const { Card } = require("../models/cardModel");

class columnController {
  async createColumn(req, res) {
    try {
      const userId = req.user._id;
      const columnName = req.body.columnName;

      const board = await Board.findOne({ _id: req.params.id });

      // Cheking for body params

      if (!columnName) {
        return res
          .status(409)
          .send({ message: "Please, enter the column name" });
      }

      // Creating the new column

      const newColumn = new Column({
        createdBy: userId,
        columnName,
        createdDate: Date.now(),
        board: board._id,
      });

      await newColumn.save();

      // board.columns.push(newColumn)

      // await board.save()

      // Adding the new column in "column" array of the user

      // const user = await User.findOne({ _id: userId })
      // user.columns.push(newColumn)

      // await user.save()

      return res.status(200).send({ message: "Success", columns: newColumn });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getСolumns(req, res) {
    try {
      // Finding all column in boards

      const columns = await Column.find({ board: req.params.id });

      if (!columns) {
        return res.status(409).send({ message: `Columns not found` });
      }

      return res.status(200).send(columns);
    } catch (err) {
      res.status(400).send({ message: err.message });
    }
  }

  async getСolumnById(req, res) {
    try {
      // Finding column
      const column = await Column.findOne({ _id: req.params.id });

      if (!columns) {
        return res.status(409).send({ message: `Column not found` });
      }

      return res.status(200).send(column);
    } catch (err) {
      res.status(400).send({ message: err.message });
    }
  }

  async deleteColumn(req, res) {
    try {
      const userId = req.user._id;
      const columnId = req.params.id;

      // Deleting board model for this params

      const checkColumn = await Column.findOneAndRemove({
        _id: columnId,
        createdBy: userId,
      });

      if (!checkColumn) {
        return res.status(409).send({ message: `This board not found` });
      }

      const card = await Card.deleteMany({ column: columnId });

      // Deleting board in the array of current User

      // const user = await User.findOne({ _id: userId })
      // const checkColumnArray = user.columns.includes(columnId)

      // if (checkColumnArray === false) {
      //     return res.status(409).send({ message: `Current user haven't column "${columnId}"` })
      // } else {
      //     user.columns.pull(columnId)
      // }

      // await user.save()

      // const board = await Board.findOne({ _id: req.params.id })

      // board.columns.pull(columnId)

      // await board.save()

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async updateUserColumn(req, res) {
    try {
      const columnId = req.params.id;
      const newColumnName = req.body.columnName;

      if (!newColumnName) {
        return res
          .status(409)
          .send({ message: "Please, enter the new column name" });
      }

      await Column.findOneAndUpdate(
        { createdBy: req.user._id, _id: columnId },
        { columnName: newColumnName }
      );

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }
}

module.exports = new columnController();
