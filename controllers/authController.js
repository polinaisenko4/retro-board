const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { authValidator } = require("./helpers/authValidation");
const { User } = require("../models/userModel");

class AuthController {
  // Function for user registration

  async register(req, res) {
    try {
      const { username, email, password } = req.body;

      //Checking users data input (email, password)

      const { error_email } = authValidator.validate({ email });
      if (error_email) {
        return res
          .status(400)
          .send({
            message:
              "Incorrect email! Your email must be similar for this: *******@***.com",
          });
      }

      const { error_password } = authValidator.validate({ password });
      if (error_password) {
        return res
          .status(400)
          .send({
            message:
              "Incorrect password! Your password must has 4 to 16 characters",
          });
      }

      //Checking the unique of email and username

      const newUser = await User.findOne({ email });
      const newUserName = await User.findOne({ username });

      if (newUser || newUserName) {
        return res
          .status(400)
          .send({ message: "User already exists! Please login!" });
      }

      //Creating new user

      const user = new User({
        email: email,
        username: username,
        password: await bcrypt.hash(password, 10),
      });

      await user.save();

      res.status(200).send({ message: "Success registaration" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  // Function for user login

  async login(req, res) {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });

      //Checking  of having user in DataBase

      if (!user) {
        return res.status(400).send({ message: "Invalid username" });
      }

      //Comparing the entered password with the password in the database

      const checkPassword = await bcrypt.compare(password, user.password);

      if (!checkPassword) {
        return res.status(400).send({ message: "Invalid password" });
      }

      //Generating of token

      const token = jwt.sign(
        {
          userId: user._id,
          email: user.email,
        },
        process.env.SECRET_KEY
      );

      console.log(token);

      res.status(200).send({ jwt_token: `Bearer ${token}` });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }
}

module.exports = new AuthController();
