const { Board } = require("../models/boardModel");
const { Column } = require("../models/columnModel");
const { User } = require("../models/userModel");

class BoardController {
  async createBoard(req, res) {
    try {
      const userId = req.user._id;
      const boardName = req.body.boardName;
      const boardDescription = req.body.boardDescription;

      const currentUser = await User.findOne({ _id: userId });

      // Cheking for body params

      // if (!boardName || !boardDescription) {
      //     return res.status(409).send({ message: 'Please, enter the board name and description' })
      // }

      // Creating the new board

      const newBoard = new Board({
        createdBy: userId,
        createdName: currentUser.username,
        boardName,
        boardDescription,
        createdDate: Date.now(),
      });

      await newBoard.save();

      // Adding the new board in "boards" array of the user

      const user = await User.findOne({ _id: userId });
      user.boards.push(newBoard);

      await user.save();

      return res.status(200).send({ message: "Success", boards: newBoard });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getAllBoards(req, res) {
    try {
      // Finding all boards

      const boards = await Board.find({ search: true });

      if (!boards) {
        return res.status(409).send({ message: `Boards not found` });
      }

      return res.status(200).send(boards);
    } catch (err) {
      res.status(400).send({ message: e.message });
    }
  }

  async getUserBoards(req, res) {
    try {
      // Finding all boards of the current user

      const user = await User.findOne({ _id: req.user._id });
      const boards = await Board.find({ _id: { $in: user.boards } });

      return res.status(200).send({ message: "Success", boards });
    } catch (err) {
      res.status(400).send({ message: e.message });
    }
  }

  async deleteBoard(req, res) {
    try {
      const userId = req.user._id;
      const boardId = req.params.id;

      // Deleting board model for this params

      const checkBoard = await Board.findOneAndRemove({
        _id: boardId,
        createdBy: userId,
      });

      if (!checkBoard) {
        return res.status(409).send({ message: `This board not found` });
      }

      // Deleting board in the array of current User

      const user = await User.findOne({ _id: userId });
      const checkBoardArray = user.boards.includes(boardId);

      if (checkBoardArray === false) {
        return res
          .status(409)
          .send({ message: `Current user haven't board "${boardId}"` });
      } else {
        user.boards.pull(boardId);
      }

      await user.save();

      const column = await Column.deleteMany({ board: boardId });

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async updateUserBoard(req, res) {
    try {
      const boardId = req.params.id;
      const newBoardName = req.body.boardName;
      const newBoardDescription = req.body.boardDescription;

      if (!newBoardName || !newBoardDescription) {
        return res
          .status(409)
          .send({
            message: "Please, enter the new board name and description",
          });
      }

      await Board.findOneAndUpdate(
        { createdBy: req.user._id, _id: boardId },
        { boardName: newBoardName, boardDescription: newBoardDescription }
      );

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getBoardById(req, res) {
    try {
      const id = req.params.id;

      const board = await Board.findOne({ _id: id });

      if (board) {
        res.status(200).send(board);
      } else {
        res.status(400).send({ message: "Not found" });
      }
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }
}

module.exports = new BoardController();
