const bcrypt = require("bcrypt");

const { User } = require("../models/userModel");

class UserController {
  // Function to get  user
  async getUser(req, res) {
    try {
      const userId = req.user._id;
      const userInfo = await User.findOne({ _id: userId });

      res.status(200).send(userInfo);
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  // Function for delete users by Id
  async deleteUser(req, res) {
    try {
      const userId = req.user.userId;
      await User.findByIdAndRemove(userId);
      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  // Function for change users password by Id
  async changeUserPassword(req, res) {
    try {
      const userId = req.user.userId;
      const { oldPassword, newPassword } = req.body;
      const user = await User.findOne({ _id: userId });

      //Comparing the entered password with the password in the database
      const checkOldPassword = await bcrypt.compare(oldPassword, user.password);
      if (!checkOldPassword) {
        return res.status(400).send({ message: "Invalid old password" });
      }
      //Generating new hash password
      const newPasswordHash = await bcrypt.hash(newPassword, 10);

      //Updating users password
      await User.updateOne({ _id: userId }, { password: newPasswordHash });

      res.status(200).send({ message: "Success" });
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getUsers(req, res) {
    try {
      // const limit = parseInt(req.query.limit || '0');
      // const offset = parseInt(req.query.offset || '0');

      const users = await User.find({ people: true });

      return res.status(200).send(users);
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getUserById(req, res) {
    try {
      const userId = req.params.id;
      console.log(userId);
      const user = await User.findOne({ _id: userId });

      console.log(user);

      res.status(200).send(user);
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }
}

module.exports = new UserController();
