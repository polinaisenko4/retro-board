const { User } = require("../models/userModel");
const { Board } = require("../models/boardModel");
const { Column } = require("../models/columnModel");
const Comment = require("../models/commentModel");

class CommentsController {
  async createComment(req, res) {
    try {
      const commentText = req.body.commentText;
      const userId = req.user._id;
      const cardId = req.params.id;
      const email = req.user.email;

      const newComment = new Comment({
        commentText,
        card: cardId,
        email,
        createdBy: userId,
        createdDate: Date.now(),
      });

      await newComment.save();

      return res.status(200).send(newComment);
    } catch (e) {
      res.status(400).send({ message: e.message });
    }
  }

  async getComments(req, res) {
    const cardId = req.params.id;

    const cards = await Comment.find({ search: true });
    console.log(cards);

    if (!cards) {
      return res.status(409).send({ message: `cards not found` });
    }

    return res.status(200).send(cards);
  }
  catch(err) {
    res.status(400).send({ message: err.message });
  }
}

module.exports = new CommentsController();
