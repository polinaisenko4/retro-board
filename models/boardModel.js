const mongoose = require("mongoose");

const Board = mongoose.model("Board", {
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  boardName: {
    type: String,
    required: true,
  },

  boardDescription: {
    type: String,
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },

  createdName: {
    type: String,
  },

  columns: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Column",
    },
  ],

  search: {
    type: Boolean,
    default: true,
  },
});

module.exports = { Board };
