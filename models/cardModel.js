const mongoose = require("mongoose");

const Card = mongoose.model("Card", {
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  cardText: {
    type: String,
    required: true,
  },

  column: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Column",
  },

  board: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Board",
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },

  note: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Note",
    },
  ],

  search: {
    type: Boolean,
    default: true,
  },

  email: {
    type: String,
  },
});

module.exports = { Card };
