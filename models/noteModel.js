const mongoose = require("mongoose");

const Note = mongoose.model("Note", {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
  },

  column: {
    type: mongoose.Schema.Types.ObjectId,
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = Note;
