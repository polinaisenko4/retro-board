const mongoose = require("mongoose");

const Comment = mongoose.model("Comment", {
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  commentText: {
    type: String,
    required: true,
  },

  card: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Card",
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },

  search: {
    type: Boolean,
    default: true,
  },

  email: {
    type: String,
  },
});

module.exports = Comment;
