const mongoose = require("mongoose");

const Column = mongoose.model("Column", {
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },

  columnName: {
    type: String,
    required: true,
  },

  board: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Board",
  },

  cards: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Card",
    },
  ],

  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = { Column };
