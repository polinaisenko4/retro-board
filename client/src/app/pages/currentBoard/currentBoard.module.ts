// import { AuthEffects } from './state/auth.effects';
import { EffectsModule } from "@ngrx/effects";
import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
// import { StoreModule } from '@ngrx/store';
import { CurrentBoardComponent } from "./currentBoard.component";
import { StoreModule } from "@ngrx/store";
import { COLUMNS_STATE_NAME } from "./store/currentBoard.selectors";
import { columnsReducer } from "./store/currentBoard.reducers";
import { ColumnsEffects } from "./store/currentBoard.effects";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { DragulaModule } from "ng2-dragula";
import { BrowserModule } from "@angular/platform-browser";
import { ColumnListComponent } from "src/app/components/column-list/column-list.component";
import { CardListComponent } from "src/app/components/card-list/card-list.component";
import { CommentsListComponent } from "src/app/components/comments-list/comments-list.component";

@NgModule({
  declarations: [
    CurrentBoardComponent,
    ColumnListComponent,
    CardListComponent,
    CommentsListComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    DragulaModule,
    DragDropModule,
    ReactiveFormsModule,
    StoreModule.forFeature(COLUMNS_STATE_NAME, columnsReducer),
    EffectsModule.forFeature([ColumnsEffects]),
  ],
})
export class CurrentBoardsModule {}
