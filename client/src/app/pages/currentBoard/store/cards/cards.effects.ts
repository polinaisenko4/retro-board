import { Injectable } from "@angular/core";
import { Actions, createEffect, Effect, ofType } from "@ngrx/effects";
import { Update } from "@ngrx/entity";
import { Store } from "@ngrx/store";
import { map, mergeMap, switchMap, withLatestFrom } from "rxjs/operators";
import { CardsService } from "src/app/services/cards.service";
import { ColumnsService } from "src/app/services/columns.service";
import { Card } from "src/app/shared/types/boards.type";
import {
  addCard,
  addCardSuccess,
  deleteCard,
  deleteCardSuccess,
  loadCards,
  loadCardSuccess,
  updateCard,
  updateCardSuccess,
} from "./cards.action";
import { getCards } from "./cards.selectors";

@Injectable()
export class CardsEffects {
  constructor(
    private actions$: Actions,
    private columnsService: ColumnsService,
    private cardsService: CardsService,
    private store: Store
  ) {}

  @Effect()
  loadCards$ = this.actions$.pipe(
    ofType(loadCards),
    withLatestFrom(this.store.select(getCards)),
    mergeMap(([action, card]) => {
      if (!card.length || card.length === 1) {
        return this.columnsService.getCardColumn().pipe(
          map((cards) => {
            return loadCardSuccess({ cards });
          })
        );
      }
      return "";
    })
  );

  addCards$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addCard),
      mergeMap((action) => {
        return this.cardsService.addCard(action._id, action.card).pipe(
          map((data) => {
            const card = { ...action.card, _id: data._id };
            return addCardSuccess({ card });
          })
        );
      })
    );
  });

  deleteCard$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deleteCard),
      switchMap((action) => {
        return this.cardsService.deleteCard(action._id).pipe(
          map((data) => {
            return deleteCardSuccess({ _id: action._id });
          })
        );
      })
    );
  });

  updateCards$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(updateCard),
      switchMap((action) => {
        return this.cardsService
          .updateCard(action.cardId, action.columnId)
          .pipe(
            map((data) => {
              const updatedCard: Update<Card> = {
                id: action.cardId,
                changes: {
                  column: action.columnId,
                },
              };
              return updateCardSuccess({ card: updatedCard });
            })
          );
      })
    );
  });
}
