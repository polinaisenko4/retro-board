import { createFeatureSelector, createSelector } from "@ngrx/store";
import { cardsAdapter, CardState } from "./cards.state";

export const CARDS_STATE_NAME = "cards";

export const getCardsState = createFeatureSelector<CardState>(CARDS_STATE_NAME);
export const cardsSelectors = cardsAdapter.getSelectors();

export const getCards = createSelector(getCardsState, cardsSelectors.selectAll);

export const getCardsEntities = createSelector(
  getCardsState,
  cardsSelectors.selectEntities
);

export const getCount = createSelector(
  getCardsEntities,
  (state) => state.count
);
