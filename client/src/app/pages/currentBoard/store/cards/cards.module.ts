import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { CARDS_STATE_NAME } from "./cards.selectors";
import { cardsReducer } from "./cards.reducers";
import { CardsEffects } from "./cards.effects";

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature(CARDS_STATE_NAME, cardsReducer),
    EffectsModule.forFeature([CardsEffects]),
  ],
})
export class CardsModule {}
