import { createReducer, on } from "@ngrx/store";
import {
  addCardSuccess,
  deleteCardSuccess,
  loadCardSuccess,
  refreshCardsArray,
  updateCardSuccess,
} from "./cards.action";
import { cardsAdapter, initialCardState } from "./cards.state";

const _cardsReducer = createReducer(
  initialCardState,

  on(loadCardSuccess, (state, action) => {
    return cardsAdapter.setAll(action.cards, {
      ...state,
      count: state.count + 1,
    });
  }),
  on(refreshCardsArray, (state) => {
    return cardsAdapter.removeAll(state);
  }),
  on(addCardSuccess, (state, action) => {
    return cardsAdapter.addOne(action.card, {
      ...state,
      count: state.count + 1,
    });
  }),
  on(deleteCardSuccess, (state, { _id }) => {
    return cardsAdapter.removeOne(_id, state);
  }),
  on(updateCardSuccess, (state, action) => {
    return cardsAdapter.updateOne(action.card, state);
  })
);

export function cardsReducer(state: any, action: any) {
  return _cardsReducer(state, action);
}
