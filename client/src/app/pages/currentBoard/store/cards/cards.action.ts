import { Update } from "@ngrx/entity";
import { createAction, props } from "@ngrx/store";
import { Card } from "src/app/shared/types/boards.type";

const LOAD_CARDS = "[CURRENT BOARD PAGE] Card load";
const LOAD_CARD_SUCCESS = "[CURRENT BOARD PAGE] Card load success";

const ADD_CARD = "[CURRENT BOARD PAGE] Card add";
const ADD_CARD_SUCCESS = "[CURRENT BOARD PAGE] Card add success";

const DELETE_CARD = "[CURRENT BOARD PAGE] Card delete";
const DELETE_CARD_SUCCESS = "[CURRENT BOARD PAGE] Card delete success";

export const loadCards = createAction(LOAD_CARDS);

export const loadCardSuccess = createAction(
  LOAD_CARD_SUCCESS,
  props<{ cards: Card[] }>()
);

export const refreshCardsArray = createAction("REFRESH_CARDS_ARRAY");

export const addCard = createAction(
  ADD_CARD,
  props<{ _id: string; card: any }>()
);

export const addCardSuccess = createAction(
  ADD_CARD_SUCCESS,
  props<{ card: any }>()
);

export const deleteCard = createAction(DELETE_CARD, props<{ _id: string }>());

export const deleteCardSuccess = createAction(
  DELETE_CARD_SUCCESS,
  props<{ _id: string }>()
);

export const updateCard = createAction(
  "UPDATE_CARD",
  props<{ cardId: string; columnId: string }>()
);

export const updateCardSuccess = createAction(
  "UPDATE_CARD_SUCCESS",
  props<{ card: Update<Card> }>()
);
