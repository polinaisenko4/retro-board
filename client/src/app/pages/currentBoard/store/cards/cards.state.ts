import { createEntityAdapter, EntityState } from "@ngrx/entity";

export interface CardState extends EntityState<any> {
  count: number;
}

export function selectCardId(a: any): string {
  return a._id;
}

export const cardsAdapter = createEntityAdapter<any>({
  selectId: selectCardId,
});

export const initialCardState: CardState = cardsAdapter.getInitialState({
  count: 0,
});
