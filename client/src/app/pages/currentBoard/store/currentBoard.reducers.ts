import { createReducer, on } from "@ngrx/store";
import {
  addColumnSuccess,
  deleteColumnSuccess,
  loadColumnsSuccess,
  refreshColumnArray,
} from "./currentBoards.actions";

import { columnsAdapter, initialColumnState } from "./currentBoards.state";

const _columnReducer = createReducer(
  initialColumnState,

  on(loadColumnsSuccess, (state, action) => {
    return columnsAdapter.setAll(action.columns, {
      ...state,
      count: state.count + 1,
    });
  }),
  on(addColumnSuccess, (state, action) => {
    return columnsAdapter.addOne(action.column, {
      ...state,
      count: state.count + 1,
    });
  }),
  on(deleteColumnSuccess, (state, { _id }) => {
    return columnsAdapter.removeOne(_id, state);
  }),

  on(refreshColumnArray, (state) => {
    return columnsAdapter.removeAll(state);
  })
);

export function columnsReducer(state: any, action: any) {
  return _columnReducer(state, action);
}
