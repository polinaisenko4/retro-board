import { createFeatureSelector, createSelector } from "@ngrx/store";
import { columnsAdapter, ColumnState } from "./currentBoards.state";

export const COLUMNS_STATE_NAME = "columns";

export const getColumnsState = createFeatureSelector<ColumnState>(
  COLUMNS_STATE_NAME
);
export const columnsSelectors = columnsAdapter.getSelectors();

export const getColumns = createSelector(
  getColumnsState,
  columnsSelectors.selectAll
);

export const getColumnEntities = createSelector(
  getColumnsState,
  columnsSelectors.selectEntities
);

export const getCount = createSelector(getColumnsState, (state) => state.count);
