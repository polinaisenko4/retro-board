import { Injectable } from "@angular/core";
import { Params } from "@angular/router";
import { Actions, createEffect, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { of } from "rxjs";
import {
  filter,
  map,
  mergeMap,
  switchMap,
  withLatestFrom,
} from "rxjs/operators";
import { BoardsService } from "src/app/services/boards.service";
import { ColumnsService } from "src/app/services/columns.service";
import { getColumns } from "../../currentBoard/store/currentBoard.selectors";
import {
  addColumn,
  addColumnSuccess,
  deleteColumn,
  deleteColumnSuccess,
  loadColumns,
  loadColumnsSuccess,
} from "../../currentBoard/store/currentBoards.actions";

@Injectable()
export class ColumnsEffects {
  constructor(
    private actions$: Actions,
    private columnsService: ColumnsService,
    private store: Store
  ) {}

  @Effect()
  loadColumns$ = this.actions$.pipe(
    ofType(loadColumns),
    withLatestFrom(this.store.select(getColumns)),
    mergeMap(([action, columns]) => {
      if (!columns.length || columns.length === 1) {
        return this.columnsService.getColumns(action._id).pipe(
          map((columns) => {
            return loadColumnsSuccess({ columns });
          })
        );
      }
      return "";
    })
  );

  addColumn$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addColumn),
      mergeMap((action) => {
        return this.columnsService.addColumn(action._id, action.column).pipe(
          map((data) => {
            const column = { ...action.column, _id: data.columns._id };
            return addColumnSuccess({ column });
          })
        );
      })
    );
  });

  deleteColumn$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deleteColumn),
      switchMap((action) => {
        return this.columnsService.deleteColumn(action._id).pipe(
          map((data) => {
            return deleteColumnSuccess({ _id: action._id });
          })
        );
      })
    );
  });
}
