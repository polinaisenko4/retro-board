import { createAction, props } from "@ngrx/store";


const LOAD_COMMENTS = '[CURRENT BOARD PAGE] Comments load'
const LOAD_COMMENTS_SUCCESS = '[CURRENT BOARD PAGE] Comments load success'

const ADD_COMMENTS = '[CURRENT BOARD PAGE] Comments add'
const ADD_COMMENTS_SUCCESS = '[CURRENT BOARD PAGE] Comments add success'


export const loadComments = createAction(
    LOAD_COMMENTS,
    props<{ id: any }>()
);

export const loadCommentsSuccess = createAction(
    LOAD_COMMENTS_SUCCESS,
    props<{ comments: any[] }>()
);


export const addComments = createAction(
    ADD_COMMENTS,
    props<{ _id: string, comments: any }>()
);

export const addCommentsSuccess = createAction(
    ADD_COMMENTS_SUCCESS,
    props<{ comment: any }>()
);

