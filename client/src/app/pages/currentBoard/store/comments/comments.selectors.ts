import { createFeatureSelector, createSelector } from "@ngrx/store";
import { getCardsState } from "../cards/cards.selectors";
import { commentAdapter, CommentState } from "./comments.state";


export const COMMENTS_STATE_NAME = 'cards';

export const getCommentsState = createFeatureSelector<CommentState>(COMMENTS_STATE_NAME);
export const commentsSelectors = commentAdapter.getSelectors();

export const getComments = createSelector(getCommentsState, commentsSelectors.selectAll);

export const getCommentsEntities = createSelector(
  getCommentsState,
  commentsSelectors.selectEntities
);

export const getCount = createSelector(getCommentsEntities, (state) => state.count);
