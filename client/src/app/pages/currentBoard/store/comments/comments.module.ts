import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { COMMENTS_STATE_NAME } from './comments.selectors';
import { commentsReducer } from './comments.reducers';
import { CommentsEffects } from './comments.effects';


@NgModule({
  declarations: [],
  imports: [
    StoreModule.forFeature(COMMENTS_STATE_NAME, commentsReducer),
    EffectsModule.forFeature([CommentsEffects]),

  ],
})

export class CommentsModule {}