import { createReducer, on } from "@ngrx/store";
import { addCommentsSuccess, loadCommentsSuccess } from "./comments.action";

import { commentAdapter, initialCommentState } from "./comments.state";

const _commentsReducer = createReducer(
    initialCommentState,

    on(loadCommentsSuccess, (state, action) => {
        return commentAdapter.setAll(action.comments, {
            ...state,
            count: state.count + 1,

        });
    }),
   
    on(addCommentsSuccess, (state, action) => {
        return commentAdapter.addOne(action.comment, {
            ...state,
            count: state.count + 1,
        });
    }),
);



export function commentsReducer(state: any, action: any) {
    return _commentsReducer(state, action);
}