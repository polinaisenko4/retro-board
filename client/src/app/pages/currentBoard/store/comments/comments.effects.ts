import { Injectable } from "@angular/core";
import { Actions, createEffect, Effect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { map, mergeMap, switchMap, withLatestFrom } from "rxjs/operators";
import { CommentsService } from "src/app/services/comments.service";
import { addComments, addCommentsSuccess, loadComments, loadCommentsSuccess } from "./comments.action";
import { getComments } from "./comments.selectors";


@Injectable()
export class CommentsEffects {
  constructor(
    private actions$: Actions,
    private commentsService: CommentsService,
    private store: Store
  ) { }

  @Effect()
  loadComments$ = this.actions$.pipe(
    ofType(loadComments),
    withLatestFrom(this.store.select(getComments)),
    mergeMap(([action, comment]) => {
      if (!comment.length || comment.length === 1) {
        return this.commentsService.getComments().pipe(
          map((comments) => {
            return loadCommentsSuccess({ comments });
          })
        );
      }
      return '';
    })
  );

  addComments$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addComments),
      mergeMap((action) => {
        return this.commentsService.addComments(action._id, action.comments).pipe(
          map((data) => {
            const comment = { ...action.comments, _id: data._id };
            return addCommentsSuccess({ comment });
          })
        );
      })
    )
  });
}