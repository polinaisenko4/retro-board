import { createEntityAdapter, EntityState } from "@ngrx/entity";

export interface CommentState extends EntityState<any> {
    count: number;
}

export function selectCommentId(a: any): string {
    return a._id;
  }

export const commentAdapter = createEntityAdapter<any>({
    selectId: selectCommentId,
});

export const initialCommentState: CommentState = commentAdapter.getInitialState({
    count: 0,
});