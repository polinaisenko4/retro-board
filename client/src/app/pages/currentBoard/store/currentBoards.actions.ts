import { createAction, props } from "@ngrx/store";
import { Column } from "src/app/shared/types/boards.type";
// import { Board } from "src/app/shared/types/boards.type";

const LOAD_COLUMNS = "[CURRENT BOARD PAGE] Column load";
const LOAD_COLUMN_SUCCESS = "[CURRENT BOARD PAGE] Column load success";

const ADD_COLUMN = "[CURRENT BOARD PAGE] Column add";
const ADD_COLUMN_SUCCESS = "[CURRENT BOARD PAGE] Column add success";

const DELETE_COLUMN = "[CURRENT BOARD PAGE] Column delete";
const DELETE_COLUMN_SUCCESS = "[CURRENT BOARD PAGE] Column delete success";

const LOAD_COLUMN_CARDS = "[CURRENT BOARD PAGE] Card load";
const LOAD_COLUMN_CARD_SUCCESS = "[CURRENT BOARD PAGE] Card load success";

const REFRESH_COLUMN_ARRAY = "[CURRENT BOARD PAGE] REFRESH COLUMN ARRAY";

export const loadColumns = createAction(LOAD_COLUMNS, props<{ _id: string }>());

export const loadColumnsSuccess = createAction(
  LOAD_COLUMN_SUCCESS,
  props<{ columns: Column[] }>()
);

export const addColumn = createAction(
  ADD_COLUMN,
  props<{ _id: string; column: Column }>()
);

export const addColumnSuccess = createAction(
  ADD_COLUMN_SUCCESS,
  props<{ column: Column }>()
);

export const deleteColumn = createAction(
  DELETE_COLUMN,
  props<{ _id: string }>()
);

export const deleteColumnSuccess = createAction(
  DELETE_COLUMN_SUCCESS,
  props<{ _id: string }>()
);

export const refreshColumnArray = createAction(REFRESH_COLUMN_ARRAY);
