import { state } from "@angular/animations";
import { EntityState, createEntityAdapter } from "@ngrx/entity";

export interface ColumnState extends EntityState<any> {
  count: number;
}

export function selectBoardId(a: any): string {
  return a._id;
}

export const columnsAdapter = createEntityAdapter<any>({
  selectId: selectBoardId,
});

export const initialColumnState: ColumnState = columnsAdapter.getInitialState({
  count: 0,
});
