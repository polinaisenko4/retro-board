import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { select, Store } from "@ngrx/store";
import { string } from "joi";
import { DragulaService } from "ng2-dragula";
import { Observable, of, Subscription, throwError } from "rxjs";
import { catchError, map, mergeMap, switchMap } from "rxjs/operators";
import { BoardsService } from "src/app/services/boards.service";
import { CardsService } from "src/app/services/cards.service";
import { ColumnsService } from "src/app/services/columns.service";
import { CommentsService } from "src/app/services/comments.service";
import { Board, Card, Column } from "src/app/shared/types/boards.type";
import {
  addCard,
  deleteCard,
  loadCards,
  loadCardSuccess,
  refreshCardsArray,
  updateCard,
} from "./store/cards/cards.action";
import { getCards } from "./store/cards/cards.selectors";
import { loadComments } from "./store/comments/comments.action";
import { getComments } from "./store/comments/comments.selectors";
import { getColumns } from "./store/currentBoard.selectors";
import {
  addColumn,
  deleteColumn,
  loadColumns,
  refreshColumnArray,
} from "./store/currentBoards.actions";

@Component({
  selector: "app-boards",
  templateUrl: "./currentBoard.component.html",
  styleUrls: ["./currentBoard.component.scss"],
})
export class CurrentBoardComponent implements OnInit {
  board: any;
  columns!: Observable<Column[]>;
  comments!: any[];
  cards!: Observable<Card[]>;
  boards!: Observable<Board[]>;

  idBoard!: string;
  idColumn!: string;
  columnId!: string;
  idCard!: string;

  columnForm!: FormGroup;
  cardForm!: FormGroup;
  commentForm!: FormGroup;

  cardDragula!: any;

  open: boolean = false;
  openComment: boolean = false;

  dragula$ = new Subscription();

  constructor(
    private boardsService: BoardsService,
    private columnsService: ColumnsService,
    private cardsService: CardsService,
    private commentsService: CommentsService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store,
    private dragulaService: DragulaService
  ) {}

  ngOnInit() {
    this.route.params
      .pipe(
        mergeMap((params: Params) => {
          return this.boardsService.getBoardById(params["id"]);
        })
      )
      .pipe(
        mergeMap((board: Board) => {
          this.board = board;
          this.idBoard = board._id;
          this.store.dispatch(loadColumns({ _id: board._id }));

          return (this.columns = this.store.select(getColumns));
        })
      )
      .pipe(
        mergeMap((columns: any) => {
          this.store.dispatch(loadCards());
          return (this.cards = this.store.select(getCards));
        })
      )
      .subscribe((cards: any) => {
        this.idCard = cards._id;
        this.commentsService.getComments().subscribe((com) => {
          this.comments = com;
        });
        return cards;
      });

    this.columnForm = new FormGroup({
      columnName: new FormControl("", [Validators.required]),
    });

    this.cardForm = new FormGroup({
      cardText: new FormControl("", [Validators.required]),
    });

    this.commentForm = new FormGroup({
      commentText: new FormControl("", [Validators.required]),
    });

    this.dragula$.add(
      this.dragulaService
        .drop("CARDS")
        .subscribe(({ name, el, target, source, sibling }) => {
          this.store.dispatch(
            updateCard({
              cardId: el.firstElementChild!.id,
              columnId: target.id,
            })
          );
        })
    );
  }

  onAddColumn() {
    const column: Column = {
      columnName: this.columnForm.value.columnName,
    };

    this.store.dispatch(addColumn({ _id: this.idBoard, column }));
    this.columnForm.reset();
  }

  onDeleteColumn(_id: string) {
    this.store.dispatch(deleteColumn({ _id }));
  }

  onAddCard() {
    const card: any = {
      cardText: this.cardForm.value.cardText,
      column: this.columnId,
      board: this.idBoard,
    };
    this.open = false;
    this.store.dispatch(addCard({ _id: this.columnId, card }));
    this.cardForm.reset();
  }

  onAddComment(card: any) {
    const comment = { commentText: this.commentForm.value.commentText };
    console.log(card);
    this.commentsService.addComments(card, comment).subscribe((comment) => {
      this.comments.push(comment);
    });
    this.commentForm.reset();
  }

  onDeleteCard(_id: string) {
    this.store.dispatch(deleteCard({ _id }));
  }

  openForm(id: string) {
    this.open = true;
    this.columnId = id;
  }

  closeForm() {
    this.open = false;
    this.columnId = "";
  }

  showComment(id: string) {
    // this.store.dispatch(loadComments({id: id}));
    // this.store.select(getComments).subscribe((comments: any) => {
    //   console.log(comments)
    // })
  }
}
