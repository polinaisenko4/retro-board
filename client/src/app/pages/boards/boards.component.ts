import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { BoardsService } from "src/app/services/boards.service";
import { refreshCardsArray } from "../currentBoard/store/cards/cards.action";
import { refreshColumnArray } from "../currentBoard/store/currentBoards.actions";
// import { Board } from "src/app/shared/types/boards.type";
import { addBoard, loadBoards } from "./store/boards.actions";
import { getBoards } from "./store/boards.selectors";

@Component({
  selector: "app-boards",
  templateUrl: "./boards.component.html",
  styleUrls: ["./boards.component.scss"],
})
export class BoardsComponent implements OnInit {
  boards!: Observable<any[]>;
  postForm!: FormGroup;

  constructor(
    private boardsService: BoardsService,
    private store: Store,
    private router: Router
  ) {}

  ngOnInit() {
    this.postForm = new FormGroup({
      boardName: new FormControl("", [Validators.required]),
      boardDescription: new FormControl("", [Validators.required]),
    });

    this.boards = this.store.select(getBoards);
    this.store.dispatch(loadBoards());

    this.store.dispatch(refreshColumnArray());
  }

  onAddBoard() {
    const board: any = {
      boardName: this.postForm.value.boardName,
      boardDescription: this.postForm.value.boardDescription,
    };

    this.store.dispatch(addBoard({ board }));
  }
}
