import { addBoardSuccess, loadBoardsSuccess, } from './boards.actions';
import { createReducer, on } from '@ngrx/store';
import { initialState, boardsAdapter } from './boards.state';

const _boardReducer = createReducer(
    initialState,
    on(loadBoardsSuccess, (state, action) => {
        return boardsAdapter.setAll(action.boards, {
            ...state,
            count: state.count + 1,
        });
    }),
    on(addBoardSuccess, (state, action) => {
        return boardsAdapter.addOne(action.board,  {
            ...state,
            count: state.count + 1,
        });
    }),
);

export function boardsReducer(state: any, action: any) {
    return _boardReducer(state, action);
}
