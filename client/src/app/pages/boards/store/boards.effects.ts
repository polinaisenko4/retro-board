import { Injectable } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { Actions, createEffect, Effect, ofType } from "@ngrx/effects";
import { RouterNavigatedAction, ROUTER_NAVIGATION } from "@ngrx/router-store";
import { Store } from "@ngrx/store";
import { of } from "rxjs";
import { filter, map, mergeMap, switchMap, withLatestFrom } from "rxjs/operators";
import { BoardsService } from "src/app/services/boards.service";
import { addBoard, addBoardSuccess, loadBoards, loadBoardsSuccess } from "./boards.actions";
import { getBoards } from "./boards.selectors";

@Injectable()
export class BoardsEffects {
  constructor(
    private actions$: Actions,
    private boardsService: BoardsService,
    private store: Store,
    private route: ActivatedRoute,
  ) { }

  @Effect()
  loadBoards$ = this.actions$.pipe(
    ofType(loadBoards),
    withLatestFrom(this.store.select(getBoards)),
    mergeMap(([action, boards]) => {
      if (!boards.length || boards.length === 1) {
        return this.boardsService.getAllBoards().pipe(
          map((boards) => {
            return loadBoardsSuccess({ boards });
          })
        );
      }
      return '';
    })
  );

  addBoard$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addBoard),
      mergeMap((action) => {
        return this.boardsService.addBoards(action.board).pipe(
          map((data) => {
            const board = { ...action.board, _id: data.boards._id };
            return addBoardSuccess({ board });
          })
        );
      })
    );
  });
}