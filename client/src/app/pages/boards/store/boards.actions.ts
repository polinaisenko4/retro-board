import { createAction, props } from "@ngrx/store";
// import { Board } from "src/app/shared/types/boards.type";

const LOAD_BOARDS = '[BOARD PAGE] Board load'
const LOAD_BOARDS_SUCCESS = '[BOARD PAGE] Board load success'

const ADD_BOARD = '[BOARD PAGE] Board add'
const ADD_BOARD_SUCCESS = '[BOARD PAGE] Board add success'


export const loadBoards = createAction(LOAD_BOARDS);
export const loadBoardsSuccess = createAction(
    LOAD_BOARDS_SUCCESS,
    props<{ boards: any[] }>()
);

export const addBoard = createAction(ADD_BOARD,
    props<{ board: any }>()
);

export const addBoardSuccess = createAction(
    ADD_BOARD_SUCCESS,
    props<{ board: any }>()
);