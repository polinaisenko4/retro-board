import { EntityState, createEntityAdapter } from '@ngrx/entity';
// import { Board } from 'src/app/shared/types/boards.type';


export interface BoardsState extends EntityState<any> {
    count: number;
}

export function selectBoardId(a: any): string {
    return a._id;
  }

export const boardsAdapter = createEntityAdapter<any>({
    selectId: selectBoardId,
});

export const initialState: BoardsState = boardsAdapter.getInitialState({
    count: 0,
});
