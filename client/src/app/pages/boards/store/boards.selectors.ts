import { createFeatureSelector, createSelector } from '@ngrx/store';
import { boardsAdapter, BoardsState } from './boards.state';

export const BOARDS_STATE_NAME = 'boards';


const getBoardsState = createFeatureSelector<BoardsState>(BOARDS_STATE_NAME);
export const boardsSelectors = boardsAdapter.getSelectors();

export const getBoards = createSelector(getBoardsState, boardsSelectors.selectAll);

export const getPostEntities = createSelector(
  getBoardsState,
  boardsSelectors.selectEntities
);


export const getCount = createSelector(getBoardsState, (state) => state.count);
