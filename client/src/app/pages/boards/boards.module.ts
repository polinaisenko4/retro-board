import { EffectsModule } from '@ngrx/effects';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { BoardsComponent } from './boards.component';
import { BOARDS_STATE_NAME } from './store/boards.selectors';
import { boardsReducer } from './store/boards.reducers'
import { BoardsEffects } from './store/boards.effects';
import { BoardListComponent } from 'src/app/components/border-create/border-list/board-list.component';
import { BoardCreateComponent } from 'src/app/components/border-create/board-create.component';

@NgModule({
  declarations: [
    BoardsComponent,
    BoardListComponent,
    BoardCreateComponent
],

  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    StoreModule.forFeature(BOARDS_STATE_NAME, boardsReducer),
    EffectsModule.forFeature([BoardsEffects]),

  ],
})
export class BoardsModule { }
