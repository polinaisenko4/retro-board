import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { map, tap } from "rxjs/operators";

import { CurrentUserInterface } from "src/app/auth/types/currentUser.interface";

import { environment } from "src/environments/environment";
import { AuthResponseInterface } from "../auth/types/authResponse.interface";
import { RegisterRequestInterface } from "../auth/types/registerRequest.interface";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  private jwt_token = null;

  constructor(private http: HttpClient) {}

  register(user: RegisterRequestInterface): Observable<CurrentUserInterface> {
    return this.http
      .post<AuthResponseInterface>(`${environment.apiUrl}/auth/register`, user)
      .pipe(map((response: AuthResponseInterface) => response.user));
  }

  login(user: RegisterRequestInterface): Observable<{ jwt_token: string }> {
    return this.http
      .post<{ jwt_token: string }>(`${environment.apiUrl}/auth/login`, user)
      .pipe(
        tap(({ jwt_token }) => {
          localStorage.setItem("jwt-token", jwt_token);
          this.setToken(jwt_token);
        })
      );
  }

  setToken(jwt_token: any) {
    this.jwt_token = jwt_token;
  }

  getToken(): any {
    return this.jwt_token;
  }

  checkToken(): boolean {
    console.log("Check token: ", !!this.jwt_token);
    return !!this.jwt_token;
  }

  logOut() {
    this.setToken(null);
    localStorage.clear();
  }
}
