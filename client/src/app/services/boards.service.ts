import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Board } from "src/app/shared/types/boards.type";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class BoardsService {
  constructor(private http: HttpClient) {}

  getAllBoards(): Observable<Board[]> {
    return this.http.get<Board[]>(`${environment.apiUrl}/boards`);
  }

  addBoards(board: any): Observable<Board> {
    return this.http.post<Board>(`${environment.apiUrl}/boards`, board);
  }

  getBoardById(id: string): Observable<Board> {
    return this.http.get<Board>(`${environment.apiUrl}/boards/${id}`);
  }
}
