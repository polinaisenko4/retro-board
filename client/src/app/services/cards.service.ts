import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { any } from "joi";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Card } from "../shared/types/boards.type";

@Injectable({
  providedIn: "root",
})
export class CardsService {
  constructor(private http: HttpClient) {}

  addCard(id: string, card: any): Observable<Card> {
    return this.http.post<Card>(`${environment.apiUrl}/cards/${id}`, card);
  }

  deleteCard(id: string): Observable<Card> {
    return this.http.delete<Card>(`${environment.apiUrl}/cards/${id}`);
  }

  getCardColumn(): Observable<Card[]> {
    return this.http.get<Card[]>(`${environment.apiUrl}/cards`);
  }

  updateCard(id: string, column: any): Observable<any> {
    return this.http.put<any>(
      `${environment.apiUrl}/cards/${id}/${column}`,
      id
    );
  }
}
