import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Card, Column } from "../shared/types/boards.type";
// import { Board } from "src/app/shared/types/boards.type"

@Injectable({
  providedIn: "root",
})
export class ColumnsService {
  constructor(private http: HttpClient) {}

  getColumns(id: string): Observable<Column[]> {
    return this.http.get<Column[]>(`${environment.apiUrl}/columns/${id}`);
  }

  getColumn(id: string): Observable<Column[]> {
    return this.http.get<Column[]>(`${environment.apiUrl}/columns/one/${id}`);
  }

  addColumn(id: string, column: any): Observable<Column> {
    return this.http.post<Column>(
      `${environment.apiUrl}/columns/${id}`,
      column
    );
  }

  deleteColumn(id: string): Observable<Column> {
    return this.http.delete<Column>(`${environment.apiUrl}/columns/${id}`);
  }

  getCardColumn(): Observable<Card[]> {
    return this.http.get<Card[]>(`${environment.apiUrl}/cards`);
  }
}
