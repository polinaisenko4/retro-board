import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root",
})
export class CommentsService {
  constructor(private http: HttpClient) {}

  getComments(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.apiUrl}/comments`);
  }

  addComments(id: any, comment: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/comments/${id}`, comment);
  }
}
