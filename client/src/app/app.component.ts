import { Component, OnInit } from "@angular/core";
import { AuthService } from "./services/auth.service";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";

@Component({
  selector: "app-root",
  template: "<router-outlet></router-outlet>",
})
export class AppComponent implements OnInit {
  constructor(private auth: AuthService) {}

  ngOnInit() {
    const token = localStorage.getItem("jwt-token");
    console.log(token);
    if (token !== null) {
      this.auth.setToken(token);
    }
  }
}
