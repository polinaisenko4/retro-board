import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { AuthService } from "src/app/services/auth.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { animate, style, transition, trigger } from "@angular/animations";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required]),
    });
  }

  onLoginSubmit() {
    this.auth.login(this.loginForm.value).subscribe(() =>
      this.router.navigate(["/boards"], {
        queryParams: {
          register: true,
        },
      })
    );
  }
}
