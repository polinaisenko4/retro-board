export interface CurrentUserInterface {
    email: string
    username: string
    password: string
}