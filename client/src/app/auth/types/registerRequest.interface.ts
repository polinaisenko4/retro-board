export interface RegisterRequestInterface {
    user: {
        email: string
        password: string
        username: string
    }
}

export interface LoginRequestInterface {
    user: {
        email: string
        password: string
    }
}