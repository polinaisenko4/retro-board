// import { AuthEffects } from './state/auth.effects';
import { EffectsModule } from "@ngrx/effects";
import { LoginComponent } from "./login/login/login.component";
import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { StoreModule } from "@ngrx/store";
import { RegisterComponent } from "./register/register.component";

@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [CommonModule, ReactiveFormsModule, EffectsModule.forFeature()],
})
export class AuthModule {}
