import { HttpErrorResponse } from "@angular/common/http";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { select, Store } from "@ngrx/store";
import { Observable, Subscription } from "rxjs";
import { AuthService } from "src/app/services/auth.service";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  isSubmitting$!: Observable<boolean>;
  aSub!: Subscription;
  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.initializeForm();
  }

  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  initializeForm() {
    console.log("initializeForm");
    this.form = this.fb.group({
      email: ["", Validators.required, Validators.email],
      username: ["", Validators.required, Validators.minLength(4)],
      password: ["", Validators.required],
    });
  }

  onSubmit() {
    this.aSub = this.auth.register(this.form.value).subscribe(
      () =>
        this.router.navigate(["/login"], {
          queryParams: {
            register: true,
          },
        }),
      () => console.log("Register success")
    );
  }
}
