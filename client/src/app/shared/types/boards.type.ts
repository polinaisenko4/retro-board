export interface Board {
  _id?: any;
  boardName: string;
  boardDescription: string;
  createdBy?: string;
  createdDate?: string;
  boards?: any;
}

export interface Column {
  _id?: any;
  createdBy?: string;
  columnName?: string;
  createdDate?: string;
  columns?: any;
}

export interface Card {
  _id?: any;
  createdBy?: string;
  cardText?: string;
  email?: string;
  column?: string;
  createdDate?: string;
}
