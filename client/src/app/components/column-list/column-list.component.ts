import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Board } from "src/app/shared/types/boards.type";

@Component({
  selector: "column-list",
  templateUrl: "./column-list.component.html",
  styleUrls: ["./column-list.component.scss"],
})
export class ColumnListComponent {
  @Input() columns: any = [];
  @Output() openForm = new EventEmitter<any>();
  @Output() deleteColumn = new EventEmitter<any>();

  showOpenForm(): void {
    this.openForm.emit();
  }

  onDeleteColumn(): void {
    this.deleteColumn.emit();
  }
}
