import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Board } from "src/app/shared/types/boards.type";

@Component({
  selector: "card-list",
  templateUrl: "./card-list.component.html",
  styleUrls: ["./card-list.component.scss"],
})
export class CardListComponent {
  @Input() cards: any = [];
  @Output() deleteCard = new EventEmitter<any>();
  @Output() showComment = new EventEmitter<any>();

  onDeleteCard(): void {
    this.deleteCard.emit();
  }

  onShowComment(): void {
    this.showComment.emit();
  }
}
