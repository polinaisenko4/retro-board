import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Board } from "src/app/shared/types/boards.type";

@Component({
  selector: "comments-list",
  templateUrl: "./comments-list.component.html",
  styleUrls: ["./comments-list.component.scss"],
})
export class CommentsListComponent {
  @Input() comments: any = [];
  @Output() openForm = new EventEmitter<any>();
  @Output() deleteColumn = new EventEmitter<any>();

  showOpenForm(): void {
    this.openForm.emit();
  }

  onDeleteColumn(): void {
    this.deleteColumn.emit();
  }
}
