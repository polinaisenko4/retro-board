import { Component, Input, Output, EventEmitter } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Board } from "src/app/shared/types/boards.type";


@Component({
    selector: 'app-board-create',
    templateUrl: './board-create.component.html',
    styleUrls: ['./board-create.component.scss'],
})

export class BoardCreateComponent {
    @Output() addBoard: EventEmitter<any> = new EventEmitter<any>();

    boardForm = new FormGroup({
        boardName: new FormControl('', [Validators.required]),
        boardDescription: new FormControl('', [Validators.required]),
    });

    onAddBoard() {
        this.addBoard.emit(this.boardForm.value)
    }
}