import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./auth/login/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { BoardsComponent } from "./pages/boards/boards.component";
import { CurrentBoardComponent } from "./pages/currentBoard/currentBoard.component";
import { AuthGuard } from "./services/auth.guard";

import { AuthLayoutComponent } from "./shared/layouts/auth-layout/auth-layout.component";
import { MainLayoutComponent } from "./shared/layouts/main-layout/main-layout.component";

const routes: Routes = [
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      { path: "", redirectTo: "register", pathMatch: "full" },
      { path: "register", component: RegisterComponent },
      { path: "login", component: LoginComponent },
    ],
  },
  {
    path: "",
    component: MainLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "boards", component: BoardsComponent },
      { path: "board/:id", component: CurrentBoardComponent },
      { path: "test", component: AppComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
