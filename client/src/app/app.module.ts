import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "../environments/environment";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { EffectsModule } from "@ngrx/effects";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthLayoutComponent } from "./shared/layouts/auth-layout/auth-layout.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { LoginComponent } from "./auth/login/login/login.component";
import { AuthModule } from "./auth/auth.module";
import { TokenInterceptor } from "./services/tokenInterseptor";
import { MainLayoutComponent } from "./shared/layouts/main-layout/main-layout.component";
import { BoardsModule } from "./pages/boards/boards.module";
import { CurrentBoardsModule } from "./pages/currentBoard/currentBoard.module";
import { appReducer } from "./shared/app.state";
import { CardsModule } from "./pages/currentBoard/store/cards/cards.module";
import { LoadingSpinnerComponent } from "./shared/loading-spinner/loading-spinner.component";
import { DragulaModule } from "ng2-dragula";
import { CommentsModule } from "./pages/currentBoard/store/comments/comments.module";

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    MainLayoutComponent,
    LoadingSpinnerComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    AuthModule,
    CardsModule,
    // CommentsModule,
    CurrentBoardsModule,
    BoardsModule,
    StoreModule.forRoot(appReducer),
    StoreModule.forRoot({}, {}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    StoreRouterConnectingModule.forRoot({}),
    EffectsModule.forRoot([]),
    DragulaModule.forRoot(),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
