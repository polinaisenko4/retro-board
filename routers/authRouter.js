const { Router } = require("express");
const router = Router();
const controllerAuth = require("../controllers/authController");

router.post("/register", controllerAuth.register);
router.post("/login", controllerAuth.login);

module.exports = router;
