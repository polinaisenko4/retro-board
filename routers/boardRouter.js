const express = require("express");
const contoller = require("../controllers/boardContoller");

const router = express.Router();

router.post("/", contoller.createBoard);
router.get("/", contoller.getAllBoards);
router.get("/users", contoller.getUserBoards);
router.delete("/:id", contoller.deleteBoard);
router.put("/:id", contoller.updateUserBoard);
router.get("/:id", contoller.getBoardById);
module.exports = router;
