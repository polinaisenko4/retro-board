const express = require("express");
const contoller = require("../controllers/commentContoller");

const router = express.Router();

router.post("/:id", contoller.createComment);
router.get("/", contoller.getComments);

module.exports = router;
