const express = require("express");
const userController = require("../controllers/userController");

const router = express.Router();

router.get("/me", userController.getUser);
router.get("/me", userController.getUsers);
router.delete("/me", userController.deleteUser);
router.patch("/me", userController.changeUserPassword);
router.get("/users", userController.getUsers);
router.get("/users/:id", userController.getUserById);

module.exports = router;
