const express = require("express");
const contoller = require("../controllers/columnContoller");

const router = express.Router();

router.post("/:id", contoller.createColumn);
router.get("/:id", contoller.getСolumns);
router.get("/one/:id", contoller.getСolumnById);
router.delete("/:id", contoller.deleteColumn);
router.put("/:id", contoller.updateUserColumn);
module.exports = router;
