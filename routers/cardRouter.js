const express = require("express");
const contoller = require("../controllers/cardContoller");

const router = express.Router();

router.post("/:id", contoller.createCard);
router.get("/", contoller.getCards);
router.delete("/:id", contoller.deleteCard);
router.put("/:idCard/:idColumn", contoller.updateCard);

module.exports = router;
